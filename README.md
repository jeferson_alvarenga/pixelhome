# PixelHome

[![CI Status](https://img.shields.io/travis/Jeferson Alvarenga/PixelHome.svg?style=flat)](https://travis-ci.org/Jeferson Alvarenga/PixelHome)
[![Version](https://img.shields.io/cocoapods/v/PixelHome.svg?style=flat)](https://cocoapods.org/pods/PixelHome)
[![License](https://img.shields.io/cocoapods/l/PixelHome.svg?style=flat)](https://cocoapods.org/pods/PixelHome)
[![Platform](https://img.shields.io/cocoapods/p/PixelHome.svg?style=flat)](https://cocoapods.org/pods/PixelHome)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PixelHome is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PixelHome'
```

## Author

Jeferson Alvarenga, jeferson.alvarenga@me.com

## License

PixelHome is available under the MIT license. See the LICENSE file for more info.
