//
//  OnboardingViewController.swift
//  PixelHome
//
//  Created by Jeferson Alvarenga on 26/08/20.
//

import UIKit
import PixelUI
import PixelTheme
import PixelOnboarding
import SnapKit
import RxCocoa
import RxSwift

class OnboardingViewController: BaseViewController {
    
    var onboardingInteractor: PixelOnboardingInteractor?
    
    typealias LottieConfig = (title: String, text: String, color: UIStyleColor, lottie: Lottie, loop: Bool)
    
    let status = BehaviorRelay(value: false)
    
    var isFirstView = true
    
    let lotties: Array<LottieConfig> = [
        (
            title: "Page usando seu celular",
            text: "Deixe a sua carteira no bolso e pague sem contato utilizando o seu celular",
            color: .skyBlue, lottie: .receipt, loop: false),
        (
            title: "#faster, #better, #safe",
            text: "Efetue pagamentos e transferências mais rápido e seguro que utilizando o seu cartão fisíco",
            color: .skyBlue, lottie: .credit_cards, loop: false),
        (
            title: "Comece agora mesmo",
            text: "Em poucos passos, você fará pagamentos de forma moderna e intuitiva",
            color: .skyBlue, lottie: .swipe_right_arrows, loop: true)
    ]
    
    
    lazy var btnOnboarding: ActionButton = {
        let btn = ActionButton(type: .forward, title: "Iniciar", action: initOnboarding)
        btn.imageColor = UIStyleColor.white
        return btn
    }()
    
    lazy var clvScreens: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let clv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        clv.backgroundColor = UIStyleColor.clear.lightColor
        clv.isPagingEnabled = true
        clv.delegate = self
        clv.dataSource = self
        clv.register(OnboardingCollectionViewCell.self, forCellWithReuseIdentifier: "reuseIdentifier")
        clv.showsHorizontalScrollIndicator = false
        clv.showsVerticalScrollIndicator = false
        return clv
    }()
    
    lazy var pageControl: UIPageControl = {
        let pgc = UIPageControl()
        pgc.numberOfPages = lotties.count
        pgc.currentPage = 0
        return pgc
    }()
    
    override func viewDidLoad() {
        title = "Pixel Pay"
        super.viewDidLoad()
        
        decoratePageControl()
        
        viewCodeSetup()
        
        clvScreens.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isFirstView {
            status.accept(true)
        }
        isFirstView = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .dark {
                viwBackground.setGradient(topColor: .deepGray, bottomColor: .black, random: false)
                btnOnboarding.color = .deepIndigo
            } else {
                viwBackground.setGradient()
                btnOnboarding.color = .deepIndigo
            }
        } else {
            viwBackground.setGradient()
        }
    }
    
    func decoratePageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [OnboardingViewController.self])
        pc.currentPageIndicatorTintColor = UIStyleColor.deepIndigo.lightColor
        pc.pageIndicatorTintColor = .gray
    }
    
    override func hierarchySetup() {
        super.hierarchySetup()
        view.addSubview(clvScreens)
        view.addSubview(pageControl)
        view.addSubview(btnOnboarding)
    }
    
    override func constraintSetup() {
        super.constraintSetup()
        
        clvScreens.snp.makeConstraints { (mkr) in
            mkr.top.equalTo(navigationView.snp.bottom).offset(20)
            mkr.width.equalToSuperview().inset(40)
            mkr.height.equalTo(500)
            mkr.centerX.equalToSuperview()
        }
        
        pageControl.snp.makeConstraints { (mkr) in
            mkr.top.equalTo(clvScreens.snp.bottom).offset(20)
            mkr.centerX.equalToSuperview()
        }
        
        btnOnboarding.snp.makeConstraints { (mkr) in
            mkr.bottom.equalToSuperview().offset(-50)
            mkr.centerX.equalToSuperview()
            mkr.width.equalToSuperview().inset(40)
        }
    }
    
    @objc func initOnboarding() {
        onboardingInteractor = PixelOnboardingInteractor(delegate: self, navigationController: navigationController)
        onboardingInteractor?.initOnboarding()
    }
}

extension OnboardingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

extension OnboardingViewController: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension OnboardingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lotties.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseIdentifier", for: indexPath as IndexPath) as? OnboardingCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setup(config: lotties[indexPath.row], observable: status)
        return cell
    }
}

class OnboardingCollectionViewCell: UICollectionViewCell {
    
    typealias LottieConfig = (title: String, text: String, color: UIStyleColor, lottie: Lottie, loop: Bool)
    
    var config: LottieConfig = (title: "", text: "", color: .blue, lottie: .credit_cards, loop: false)
    
    var observable = BehaviorRelay(value: false)
    
    let disposeBag = DisposeBag()
    
    lazy var viwContainer: View = {
        return View(color: config.color)
    }()
    
    lazy var lblTitle: Label = {
        let lbl = Label(size: .large, color: .deepIndigo, text: config.title)
        lbl.textAlignment = .center
        return lbl
    }()
    
    lazy var lblText: Label = {
        let lbl = Label(size: .normal, color: .indigo, text: config.text)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        lbl.textAlignment = .center
        return lbl
    }()
    
    lazy var viwLottie: LottieView = {
        if config.loop {
            return LottieView(source: config.lottie, loopMode: .autoReverse)
        } else {
            return LottieView(source: config.lottie, loopMode: .playOnce)
        }
    }()
    
    func setup(config: LottieConfig, observable: BehaviorRelay<Bool>) {
        self.config = config
        viewCodeSetup()
        viwLottie.backgroundColor = config.color.lightColor
        self.observable = observable
        setObservable()
    }
    
    func setObservable() {
        observable.asObservable().subscribe(onNext: { (status) in
            self.viwLottie.play()
        }).disposed(by: disposeBag)
    }
}

extension OnboardingCollectionViewCell: ViewCodeProtocol {
    
    public func hierarchySetup() {
        addSubview(viwContainer)
        viwContainer.addSubview(viwLottie)
        viwContainer.addSubview(lblTitle)
        viwContainer.addSubview(lblText)
    }
    
    public func constraintSetup() {
        
        viwContainer.snp.makeConstraints { (mkr) in
            mkr.top.bottom.equalToSuperview()
            mkr.width.equalToSuperview().inset(10)
            mkr.centerX.equalToSuperview()
        }
        
        lblTitle.snp.makeConstraints { (mkr) in
            mkr.top.equalToSuperview().offset(20)
            mkr.width.equalToSuperview()
        }
        
        lblText.snp.makeConstraints { (mkr) in
            mkr.top.equalTo(lblTitle.snp.bottom).offset(30)
            mkr.width.equalToSuperview().inset(10)
            mkr.centerX.equalToSuperview()
        }
        
        viwLottie.snp.makeConstraints { (mkr) in
            mkr.width.bottom.equalToSuperview()
            mkr.height.equalTo(frame.width)
        }
    }
}

extension OnboardingViewController: PixelOnboardingInteractorDelegate {
    
    func onOnboardingExit(object: Any?) {
        onboardingInteractor = nil
        navigationController?.popToViewController(self, animated: true)
    }
    
    func onAddCardsTapped(object: Any?) {
        onboardingInteractor = nil
        if let navigationController = self.navigationController {
            navigationController.popToViewController(self, animated: true)
        }
    }
    
    func onOnboardingCancel(notification: NSNotification) {
        onboardingInteractor = nil
        navigationController?.popToViewController(self, animated: true)
    }
}
