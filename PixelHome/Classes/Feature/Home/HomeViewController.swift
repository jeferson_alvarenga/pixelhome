//
//  HomeViewController.swift
//  PixelHome
//
//  Created by Jeferson Alvarenga on 17/08/20.
//

import PixelUI
import PixelOnboarding
import PixelCard
import PixelModel
import SnapKit

class HomeViewController: BaseViewController {
    
    let user: User
    var onboardingInteractor: PixelOnboardingInteractor?
    
    lazy var btnOnboarding: UIButton = {
        return UIButton(type: .custom)
    }()
    
    lazy var tblHome: UITableView = {
        return UITableView()
    }()
    
    init(user: User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        title = "Home"
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .dark {
                viwBackground.setGradient(topColor: .deepGray, bottomColor: .black, random: false)
            } else {
                viwBackground.setGradient()
            }
        } else {
            viwBackground.setGradient()
        }
    }
    
    override func hierarchySetup() {
        super.hierarchySetup()
        view.addSubview(btnOnboarding)
    }
    
    override func constraintSetup() {
        
        super.constraintSetup()
        
        btnOnboarding.snp.makeConstraints { (mkr) in
            mkr.top.equalTo(navigationView.snp.bottom).offset(20)
            mkr.left.equalToSuperview().offset(20)
            mkr.right.equalToSuperview().offset(-20)
            mkr.height.equalTo(50)
        }
    }
    
    override func themeSetup() {
        btnOnboarding.backgroundColor = .blue
        btnOnboarding.setTitleColor(.white, for: .normal)
    }
    
    override func additionalSetup() {
        btnOnboarding.setTitle("Iniciar onboarding", for: .normal)
        btnOnboarding.addTarget(self, action: #selector(initOnboarding), for: .touchUpInside)
    }
    
    @objc func initOnboarding() {
        onboardingInteractor = PixelOnboardingInteractor(delegate: self, navigationController: navigationController)
        onboardingInteractor?.initOnboarding()
    }
}

extension HomeViewController: PixelOnboardingInteractorDelegate {
    
    func onOnboardingExit(object: Any?) {
        onboardingInteractor = nil
        navigationController?.popToViewController(self, animated: true)
    }
    
    func onAddCardsTapped(object: Any?) {
        onboardingInteractor = nil
        if let navigationController = self.navigationController {
            navigationController.popToViewController(self, animated: true)
        }
    }
    
    func onOnboardingCancel(notification: NSNotification) {
        onboardingInteractor = nil
        navigationController?.popToViewController(self, animated: true)
    }
}

extension HomeViewController: PixelCardInteractorDelegate {
    
}
