//
//  PixelHomeInteractor.swift
//  PixelHome
//
//  Created by Jeferson Alvarenga on 17/08/20.
//

import PixelModel
import PixelUI
import PixelFoundation
import PixelExtension
import PixelOnboarding
import PixelCard

@objc public protocol PixelHomeInteractorDelegate: InteractorProtocol {
    
}

public class PixelHomeInteractor {
    
    let navigationController: UINavigationController
    var pixelCardInteractor: PixelCardInteractor?
    
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func initJourney() {
        if let user = initUserSession() {
            initHome(user: user)
        } else {
            initHome(user: User())
        }
    }
    
    func initUserSession() -> User? {
        return SessionUtils.get(.user) as? User
    }
    
    func initOnboarding(user: User) {
        
    }
    
    func initHome(user: User) {
        navigationController.pushViewController(OnboardingViewController(), animated: false)
    }
}

extension PixelHomeInteractor: PixelOnboardingInteractorDelegate {
    
    public func onOnboardingExit(object: Any?) {
        navigationController.popToRootViewController(animated: true) {
            if let notification = object as? NSNotification,
                let user = notification.object as? User {
                self.navigationController.pushViewController(HomeViewController(user: user), animated: true)
            } else {
                //TODO show/log error
            }
        }
    }
    
    
    public func onOnboardingCancel(notification: NSNotification) {
        
    }
    
    public func onAddCardsTapped(object: Any?) {
        pixelCardInteractor = PixelCardInteractor(delegate: self, navigationController: navigationController)
        pixelCardInteractor?.initAddCard()
    }
}

extension PixelHomeInteractor: PixelCardInteractorDelegate {
    
}
