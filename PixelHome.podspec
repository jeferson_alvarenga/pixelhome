#
# Be sure to run `pod lib lint PixelHome.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PixelHome'
  s.version          = '0.1.0'
  s.summary          = 'A short description of PixelHome.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/Jeferson Alvarenga/PixelHome'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jeferson Alvarenga' => 'jeferson.alvarenga@me.com' }
  s.source           = { :git => 'https://github.com/Jeferson Alvarenga/PixelHome.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'PixelHome/Classes/**/*'
  
  # s.resource_bundles = {
  #   'PixelHome' => ['PixelHome/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'SnapKit', '~> 4.0.0'
  s.dependency 'PixelPayment', '~> 0.1.0'
  s.dependency 'PixelTheme', '~> 0.1.0'
  s.dependency 'PixelUI', '~> 0.1.0'
  s.dependency 'PixelModel', '~> 0.1.0'
  s.dependency 'PixelExtension', '~> 0.1.0'
  s.dependency 'PixelFoundation', '~> 0.1.0'
  s.dependency 'PixelOnboarding', '~> 0.1.0'
  s.dependency 'PixelCard', '~> 0.1.0'
end
